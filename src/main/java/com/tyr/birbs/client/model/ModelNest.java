package com.tyr.birbs.client.model;

import net.minecraft.client.renderer.entity.model.EntityModel;
import net.minecraft.entity.Entity;

public class ModelNest<T extends Entity> extends EntityModel<T> {

    public ModelNest() {

    }

    @Override
    public void render(T entity,
                       float limbSwing,
                       float limbSwingAmount,
                       float ageInTicks,
                       float netHeadYaw,
                       float headPitch,
                       float scale) {
        this.setRotationAngles(entity, limbSwing, limbSwingAmount, ageInTicks, netHeadYaw, headPitch, scale);
    //    this.body.render(scale);
    }
}
