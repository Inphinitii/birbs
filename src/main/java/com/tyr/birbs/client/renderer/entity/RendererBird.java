package com.tyr.birbs.client.renderer.entity;

import com.tyr.birbs.Birbs;
import com.tyr.birbs.client.model.ModelBird;
import com.tyr.birbs.common.entity.EntityBird;
import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.client.renderer.entity.MobRenderer;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

@OnlyIn(Dist.CLIENT)
public class RendererBird extends MobRenderer<EntityBird, ModelBird<EntityBird>> {

    private static final ResourceLocation BLUE_TEXTURE  = new ResourceLocation(Birbs.MOD_ID,
                                                                               "textures/entity/bird_blue.png");

    private static final ResourceLocation RED_TEXTURE   = new ResourceLocation(Birbs.MOD_ID,
                                                                               "textures/entity/bird_red.png");

    private static final ResourceLocation GREEN_TEXTURE = new ResourceLocation(Birbs.MOD_ID,
                                                                               "textures/entity/bird_green.png");

    public RendererBird(EntityRendererManager _manager) {
        super(_manager, new ModelBird<>(), 1.0F);
        this.shadowSize = 0.2F;
    }

    @Override
    protected void preRenderCallback(EntityBird entityBird, float partialTickTime) {
    }

    @Override
    protected ResourceLocation getEntityTexture(EntityBird entityBird) {
        switch(entityBird.getBirdType()) {
            default:
            case 1:
                return RED_TEXTURE;
            case 2:
                return GREEN_TEXTURE;
            case 3:
                return BLUE_TEXTURE;
        }
    }
}
