package com.tyr.birbs.client.model;

import net.minecraft.client.renderer.entity.model.EntityModel;
import net.minecraft.client.renderer.entity.model.RendererModel;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.MobEntity;

/**
 * boar - cybercat5555 Created using Tabula 5.1.0
 */
public class ModelBird<T extends LivingEntity> extends EntityModel<T> {

    public RendererModel body;
    public RendererModel head;
    public RendererModel beak;
    public RendererModel tail;
    public RendererModel lwing;
    public RendererModel rwing;

    public ModelBird() {
        this.textureWidth  = 32;
        this.textureHeight = 32;

        this.body  = new RendererModel(this, 0, 0);
        this.head  = new RendererModel(this, 16, 0);
        this.beak  = new RendererModel(this, 16, 5);
        this.lwing = new RendererModel(this, 6, 8);
        this.rwing = new RendererModel(this, 0, 8);
        this.tail  = new RendererModel(this, 12, 8);

        this.body.offsetY = 1.25F;
        this.body.offsetX = -0.18F;
        this.body.offsetZ = -0.18F;

        this.body.setRotationPoint(0.0F, 0.0F, 0.0F);
        this.head.setRotationPoint(0.0F, -0.5F, 0.0F);

        this.lwing.setRotationPoint(0.0F, -0.15F, 0.0F);
        this.rwing.setRotationPoint(0.0F, -0.15F, 0.0F);

        this.head.addBox(  0.5F , -2.0F  , 0.5F, 3, 2, 3, 0.0F);
        this.beak.addBox(  1.5F , -1.2F  ,-0.5F, 1, 1, 1, 0.0F);
        this.body.addBox(  0.0F ,  0.0F  , 0.0F, 4, 4, 4, 0.0F);
        this.lwing.addBox( 0.1F , -0.15F , 0.0F, 1, 2, 3, 0.0F);
        this.rwing.addBox(-0.1F , -0.15F , 0.0F,-1, 2, 3, 0.0F); //THIS IS NOT HOW IT SHOULD BE DONE BUT HEY
        this.tail.addBox(  1.5F ,  2.5F  , 3.5F, 1, 1, 2, 0.0F);

        this.lwing.offsetX = 0.25F;

        this.head.addChild(this.beak);
        this.body.addChild(this.head);
        this.body.addChild(this.tail);
        this.body.addChild(this.lwing);
        this.body.addChild(this.rwing);
    }

    @Override
    public void render(T entity,
                       float limbSwing,
                       float limbSwingAmount,
                       float ageInTicks,
                       float netHeadYaw,
                       float headPitch,
                       float scale) {
        this.body.render(scale);
    }

    @Override
    public void setRotationAngles(T entity,
                       float limbSwing,
                       float limbSwingAmount,
                       float ageInTicks,
                       float netHeadYaw,
                       float headPitch,
                       float scale) {
        super.setRotationAngles(entity, limbSwing, limbSwingAmount, ageInTicks, netHeadYaw, headPitch, scale);

        //Taken from ChickenModel.java
        this.head.rotateAngleX  = headPitch * ((float)Math.PI / 180F) * 0.7453292F;
        this.lwing.rotateAngleZ = ((float)Math.sin(-ageInTicks  * 0.10f) / 2) - 0.5F;
        this.rwing.rotateAngleZ = ((float)Math.sin( ageInTicks  * 0.10f) / 2) + 0.5F;

    }
}
