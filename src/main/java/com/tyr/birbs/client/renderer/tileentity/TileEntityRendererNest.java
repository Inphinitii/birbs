package com.tyr.birbs.client.renderer.tileentity;

import com.tyr.birbs.common.tileentity.TileEntityNest;
import net.minecraft.client.renderer.tileentity.TileEntityRenderer;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

@OnlyIn(Dist.CLIENT)
public class TileEntityRendererNest extends TileEntityRenderer<TileEntityNest> {

    //private ModelExample<TileEntityExample> mainModel;

    public TileEntityRendererNest() {
    }

    @Override
    public void render(TileEntityNest entity, double x, double y, double z, float particleTicks, int destroyStage) {

    }
}
