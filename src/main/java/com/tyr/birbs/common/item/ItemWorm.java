package com.tyr.birbs.common.item;

import net.minecraft.item.Food;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;

/**
 * Worms are going to be small entities that spawn on the ground and are targets for the ${@link EntityBird} entities.
 * If the player gathers one of these worms, they can be used in a ${@link BlockNest} to attract a bird which is uniquely
 * tamable as opposed to the bird entities that spawned in the wild.
 *
 *
 * The EntityWorms may also be better suited as a modified loot table to dirt. This would reduce the impact the mod
 * has on the server and in singleplayer performance, since less entities would be rendered by the game.
 * Also may make it a little more controllable and tunable.
 */
public class ItemWorm extends Item {

    private static final Food WORM = (new Food.Builder()).hunger(1).saturation(0.5F).build();

    public ItemWorm() {
        super(getProperties());
    }

    private static Properties getProperties() {
        return new Item.Properties().food(WORM).group(ItemGroup.FOOD);
    }
}
