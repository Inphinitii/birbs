package com.tyr.testing.client.model;

import net.minecraft.client.renderer.entity.model.EntityModel;
import net.minecraft.client.renderer.entity.model.RendererModel;
import net.minecraft.entity.LivingEntity;

//TODO Spawn LARGE, FAT birds at extremely rare intervals. Think slimes with heads.
public class ModelTest<T extends LivingEntity> extends EntityModel<T> {

    public RendererModel main;

    public ModelTest(){
        this.textureWidth  = 32;
        this.textureHeight = 32;
        this.main = new RendererModel(this, 0, 0);

        int width = 4, height = 4, length = 4;
        this.main.addBox(0.0F - (width / 2.0F), 23.0F - height, 0.0F - (length / 2.0F), width, height, length, 1.0F);

        width  = 3;
        height = 3;
        length = 3;
        this.main.addBox(0.0F - (width / 2.0F), 23.0F - height - 6, 0.0F - (length / 2.0F), width, height, length, 1.0F);
    }


    @Override
    public void render(T entity,
                       float limbSwing,
                       float limbSwingAmount,
                       float ageInTicks,
                       float netHeadYaw,
                       float headPitch,
                       float scale) {
        this.setRotationAngles(entity, limbSwing, limbSwingAmount, ageInTicks, netHeadYaw, headPitch, scale);
        this.main.render(scale);
    }
}
