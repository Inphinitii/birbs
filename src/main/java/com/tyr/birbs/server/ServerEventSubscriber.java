package com.tyr.birbs.server;

import com.tyr.birbs.Birbs;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.server.FMLServerStartingEvent;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/*
 * The ServerEventSubscriber is responsible for hooking server-side events and registering new server commands or
 * functionality.
 */
@Mod.EventBusSubscriber(modid = Birbs.MOD_ID,
                        bus   = Mod.EventBusSubscriber.Bus.MOD)
public final class ServerEventSubscriber {

    private static final Logger LOGGER = LogManager.getLogger();

    @SubscribeEvent
    public static void onServerStart(FMLServerStartingEvent event) {
        LOGGER.info("HELLO from server starting");
    }

}
