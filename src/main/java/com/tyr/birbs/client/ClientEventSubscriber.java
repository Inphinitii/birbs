package com.tyr.birbs.client;

import com.tyr.birbs.Birbs;
import com.tyr.birbs.client.renderer.entity.RendererBird;
import com.tyr.birbs.client.renderer.tileentity.TileEntityRendererNest;
import com.tyr.birbs.common.entity.EntityBird;
import com.tyr.birbs.common.tileentity.TileEntityNest;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.client.registry.ClientRegistry;
import net.minecraftforge.fml.client.registry.RenderingRegistry;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;

/*
 * The ClientEventSubscriber is responsible for hooking client-side events and registering all necessary resources such
 * as models and renderers
 */
@Mod.EventBusSubscriber(modid = Birbs.MOD_ID,
                        value = Dist.CLIENT,
                        bus   = Mod.EventBusSubscriber.Bus.MOD)
public class ClientEventSubscriber {

    @SubscribeEvent
    public static void onClientSetup(final FMLClientSetupEvent event) {
        ClientRegistry.bindTileEntitySpecialRenderer(TileEntityNest.class, new TileEntityRendererNest());

        RenderingRegistry.registerEntityRenderingHandler(EntityBird.class, RendererBird::new);
    }

}
