package com.tyr.birbs.common.entity;

import net.minecraft.entity.*;
import net.minecraft.entity.ai.goal.*;
import net.minecraft.entity.passive.IFlyingAnimal;
import net.minecraft.entity.passive.ShoulderRidingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.*;
import net.minecraft.world.World;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.biome.Biomes;

import javax.annotation.Nullable;
import java.util.Random;

public class EntityBird extends ShoulderRidingEntity implements IFlyingAnimal {

    private int type = 0;

    public EntityBird(EntityType<? extends EntityBird> _type, World _world) {
        super(_type, _world);
        type = new Random().nextInt(3);
        stepHeight = 1.0F;
    }

    public int getBirdType() {
       return type;
    }

    public static EntityType.Builder<EntityBird> getBuilder() {
        return EntityType.Builder.create(EntityBird::new, EntityClassification.CREATURE).size(0.4F, 0.5F);
    }


    /*@Override
    public EntitySize getSize(Pose _in) {
        return super.getSize(_in).scale(0.5f, 0.25f);
    }*/

    @Override
    protected void registerGoals() {
        this.goalSelector.addGoal(1, new WaterAvoidingRandomFlyingGoal(this, 1.0D));
        this.goalSelector.addGoal(0, new LookAtGoal(this, PlayerEntity.class, 10.0F));
    }

    @Override
    protected void registerAttributes() {
       super.registerAttributes();
       this.getAttributes().registerAttribute(SharedMonsterAttributes.FLYING_SPEED);

       this.getAttribute(SharedMonsterAttributes.MAX_HEALTH).setBaseValue(4.0D);
       this.getAttribute(SharedMonsterAttributes.MOVEMENT_SPEED).setBaseValue(0.2D);
       this.getAttribute(SharedMonsterAttributes.FOLLOW_RANGE).setBaseValue(20.0D);
       this.getAttribute(SharedMonsterAttributes.FLYING_SPEED).setBaseValue(0.4D);
    }

    @Override
    protected void setupTamedAI() {
    }

    @Override
    public void tick() {
        super.tick();
    }

    @Override
    public void livingTick() {
        super.livingTick();
    }

    @Override
    protected SoundEvent getAmbientSound() {
        return SoundEvents.ENTITY_CHICKEN_AMBIENT; //TODO CHANGE
    }

    @Override
    protected SoundEvent getHurtSound(DamageSource _source) {
        return SoundEvents.ENTITY_CHICKEN_HURT; //TODO CHANGE
    }

    @Override
    protected SoundEvent getDeathSound() {
        return SoundEvents.ENTITY_CHICKEN_DEATH; //TODO CHANGE
    }

    @Nullable
    @Override
    public AgeableEntity createChild(AgeableEntity ageableEntity) {
        return null;
    }

    @Override
    public boolean processInteract(PlayerEntity _player, Hand _hand) {
        return false;
    }

    @Override
    public boolean isInvulnerableTo(DamageSource _source) {
       return false;
    }

    @Override
    public boolean canDespawn(double range) {
        return !this.hasCustomName();
    }
}
