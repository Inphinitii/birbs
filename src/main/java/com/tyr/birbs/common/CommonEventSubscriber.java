package com.tyr.birbs.common;

import com.tyr.birbs.Birbs;
import com.tyr.birbs.common.entity.EntityBird;
import com.tyr.birbs.common.item.ItemWorm;
import net.minecraft.block.Block;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityClassification;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.MobEntity;
import net.minecraft.item.Item;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.biome.Biomes;
import net.minecraftforge.common.BiomeDictionary;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.IForgeRegistryEntry;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

/*
 * The CommonEventSubscriber is responsible for hooking into events shared between both the CLIENT and SERVER.
 * This includes functionality such as IMC, allowing for mods to communicate with one-another.
 */
@Mod.EventBusSubscriber(modid = Birbs.MOD_ID,
                        bus   = Mod.EventBusSubscriber.Bus.MOD)
public final class CommonEventSubscriber {

    private static final Logger LOGGER = LogManager.getLogger();

    //TODO externalize the loading of items/properties.
    @SubscribeEvent
    public static void onRegisterItems(RegistryEvent.Register<Item> event) {

        final Item itemWorm = create(new ItemWorm(), "worm");

        event.getRegistry().registerAll(
                itemWorm
        );
    }

    //TODO externalize the loading of entity.
    @SubscribeEvent
    public static void onRegisterEntities(RegistryEvent.Register<EntityType<?>> event) {

        final EntityType<EntityBird> entityBird = build("bird", EntityBird.getBuilder());

        event.getRegistry().registerAll(
              entityBird
        );

        addSpawn(entityBird, 100, 2, 4,
                EntityClassification.CREATURE,
                Biomes.FOREST,
                Biomes.PLAINS,
                Biomes.BIRCH_FOREST,
                Biomes.BIRCH_FOREST_HILLS,
                Biomes.DARK_FOREST,
                Biomes.DARK_FOREST_HILLS);
    }

    @SubscribeEvent
    public static void onRegisterBlocks(final RegistryEvent.Register<Block> blockRegistryEvent) {
    }

    /**
     * The following couple helper functions were taken from:
     * https://github.com/Choonster-Minecraft-Mods/TestMod3/blob/1.14.4/src/main/java/choonster/testmod3/init/ModEntities.java
     *
     * Build an {@link EntityType} from a {@link EntityType.Builder} using the specified name.
     *
     * @param name    The entity type name
     * @param builder The entity type builder to build
     * @return The built entity type
     */
    private static <T extends Entity> EntityType<T> build(final String name, final EntityType.Builder<T> builder) {
        final ResourceLocation registryName = new ResourceLocation(Birbs.MOD_ID, name);
        final EntityType<T>    entityType   = builder.build(registryName.toString());

        entityType.setRegistryName(registryName);
        return entityType;
    }

    /**
     * A tiny wrapper function used to properly create and register an {@link IForgeRegistryEntry} object using our
     * MOD_ID
     *
     * @param entry The IForgeRegistryEntry object
     * @param name The name to enter the object into the registry as
     * @return IForgeRegistryEntry object
     */
    private static <T extends IForgeRegistryEntry<T>> T create(final T entry, final String name) {
        return create(entry, new ResourceLocation(Birbs.MOD_ID, name));
    }

    /**
     * A tiny wrapper function used to properly create and register an {@link IForgeRegistryEntry} object using our
     * MOD_ID. Typically it's better to call the create(T, String) method.
     *
     * @param entry The IForgeRegistryEntry object
     * @param registryName The {@link ResourceLocation} object to set in the registry.
     * @return IForgeRegistryEntry object
     */
    private static <T extends IForgeRegistryEntry<T>> T create(final T entry, final ResourceLocation registryName) {
        entry.setRegistryName(registryName);
        return entry;
    }

    /**
     * Add a spawn entry for the supplied entity in the supplied {@link Biome} list.
     * <p>
     * Adapted from Forge's {@code EntityRegistry.addSpawn} method in 1.12.2.
     *
     * @param entityType     The entity type
     * @param itemWeight     The weight of the spawn list entry (higher weights have a higher chance to be chosen)
     * @param minGroupCount  Min spawn count
     * @param maxGroupCount  Max spawn count
     * @param classification The entity classification
     * @param biomes         The biomes to add the spawn to
     */
    private static void addSpawn(final EntityType<? extends MobEntity> entityType, final int itemWeight, final int minGroupCount, final int maxGroupCount, final EntityClassification classification, final Biome... biomes) {
        for (final Biome biome : biomes) {
            final List<Biome.SpawnListEntry> spawns = biome.getSpawns(classification);

            // Try to find an existing entry for the entity type
            spawns.stream()
                    .filter(entry -> entry.entityType.equals(entityType))
                    .findFirst()
                    .ifPresent(spawns::remove); // If there is one, remove it

            // Add a new one
            spawns.add(new Biome.SpawnListEntry(entityType, itemWeight, minGroupCount, maxGroupCount));
        }
    }

    /**
     * Get an array of {@link Biome}s with the specified {@link BiomeDictionary.Type}.
     *
     * @param type The Type
     * @return An array of Biomes
     */
    private static Biome[] getBiomes(final BiomeDictionary.Type type) {
        return BiomeDictionary.getBiomes(type).toArray(new Biome[0]);
    }
}
