package com.tyr.birbs;

import net.minecraft.util.ResourceLocation;
import net.minecraft.world.storage.loot.LootPool;
import net.minecraft.world.storage.loot.TableLootEntry;
import net.minecraftforge.event.LootTableLoadEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;

@Mod.EventBusSubscriber(modid = Birbs.MOD_ID,
                        bus   = Mod.EventBusSubscriber.Bus.FORGE)
public class ForgeEventSubscriber {

    @SubscribeEvent
    public static void onLootLoad(LootTableLoadEvent event) {
        if (event.getName().equals(new ResourceLocation("minecraft", "blocks/grass_block"))) {
            event.getTable().addPool(
                    LootPool.builder().addEntry(
                            TableLootEntry.builder(new ResourceLocation(Birbs.MOD_ID, "blocks/grass_block"))
                    ).build()
            );
        }
    }
}
